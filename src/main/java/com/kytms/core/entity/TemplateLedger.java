package com.kytms.core.entity;

import com.alibaba.fastjson.annotation.JSONField;
import org.apache.log4j.Logger;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * 辽宁捷畅物流有限公司 -信息技术中心
 * 模板台帐表
 * @author 陈小龙
 * @create 2018-04-09
 */
@Entity(name = "JC_TEMPLATE_LEDGER")
public class TemplateLedger extends BaseEntityNoCode implements Serializable {
    private double amount; //金额
    private double taxRate; //税率
    private double input; //税金
    private int type;//类型
    private ShipmentTemplate  shipmentTemplate;  //运单模板
    private Organization organization; //组织机构
    private List<TemplateLedgerDetails> templateLedgerDetails;  //模板台账明细


    private final Logger log = Logger.getLogger(TemplateLedger.class);//输出Log日志


    @Column(name = "AMOUNT")
    public double getAmount() {
        return amount;
    }
    public void setAmount(double amount) {
        this.amount = amount;
    }

    @Column(name = "TAX_RATE")
    public double getTaxRate() {
        return taxRate;
    }
    public void setTaxRate(double taxRate) {
        this.taxRate = taxRate;
    }

    @Column(name = "INPUT")
    public double getInput() {
        return input;
    }
    public void setInput(double input) {
        this.input = input;
    }

    @Column(name = "TYPE")
    public int getType() {
        return type;
    }
    public void setType(int type) {
        this.type = type;
    }

    @ManyToOne(cascade={CascadeType.PERSIST })
    @JoinColumn(name = "JC_ORGANIZATION_ID")
    public Organization getOrganization() {
        return organization;
    }
    public void setOrganization(Organization organization) {
        this.organization = organization;
    }

    @OneToMany(cascade = { CascadeType.ALL},mappedBy ="templateLedger")
    public List<TemplateLedgerDetails> getTemplateLedgerDetails() {
        return templateLedgerDetails;
    }
    public void setTemplateLedgerDetails(List<TemplateLedgerDetails> templateLedgerDetails) {
        this.templateLedgerDetails = templateLedgerDetails;
    }

    @JSONField(serialize=false)
    @ManyToOne(cascade={CascadeType.PERSIST })
    @JoinColumn(name = "JC_SHIPMENT_TEMPLATE_ID")
    public ShipmentTemplate getShipmentTemplate() {
        return shipmentTemplate;
    }
    public void setShipmentTemplate(ShipmentTemplate shipmentTemplate) {
        this.shipmentTemplate = shipmentTemplate;
    }
}
